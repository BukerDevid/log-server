from os import error
import sys

from flask.wrappers import Response
sys.path.append('database')
from database.database import Database
from flask import Flask, request, render_template, redirect
# _____________________ #
HOST='192.168.1.201:5000'
# _____________________ #
app = Flask(__name__)

print('Start server LOGS server')
base = Database()
base.Check_database()
base.DefaultDataSet()

@app.route('/')
def index():
    db = Database()
    token = request.cookies.get('session')
    print('token -', token)
    data = db.SelectCheckToken(token)
    if data.Error:
        return redirect("http://{host}/login".format(host=HOST), code=302)
    else:
        return redirect("http://{host}/home".format(host=HOST), code=302) 

@app.route('/home', methods=['GET','POST'])
def home():
    if request.method == 'GET':
        key = request.args.get('filter')
        db = Database()
        token = request.cookies.get('session')
        data = db.SelectCheckToken(token)
        if data.Error:
            return redirect("http://{host}/login".format(host=HOST), code=302)
        data = db.SelectLogs(key)
        return render_template('logs.html', data=data)
    else:
        return request(code=405)


@app.route('/login', methods=['GET','POST'])
def login():
    db = Database()
    token = request.cookies.get('session')
    data = db.SelectCheckToken(token)
    if request.method == 'GET':
        return render_template('login.html')
    elif request.method == 'POST':
        data = db.SelectCheckLoginAndPassword(request.form['email'], request.form['pwd'])
        if data.Error:
            return render_template('login.html', data)
        else:
            resp = redirect("http://{host}/home".format(host=HOST), code=302)
            resp.set_cookie('session',data.Data)
            return resp

@app.route('/set-log', methods=['POST'])
def set_log():
    if request.method == 'POST':
        db = Database()
        key = request.args.get('key')
        server = request.args.get('srv')
        logs = request.args.get('logs')
        if key == 'Yud893I9ueujjie209eFFJdjue8':
            db.InsertLogs(server,logs)
            return 'Success!'
        else:
            return request(code=401)
    return request(code=405)
