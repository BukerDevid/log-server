class StatisticNote:
    NewNote = int
    ActiveNote = int
    ReadyNote = int
    CancelNote = int
    TimingWork = int
    
    def __init__(self, NewNote, ActiveNote, ReadyNote, CancelNote, TimingWork):
        self.NewNote = NewNote
        self.ActiveNote = ActiveNote
        self.ReadyNote = ReadyNote
        self.CancelNote = CancelNote
        self.TimingWork = TimingWork
