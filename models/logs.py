class Logs:
    ID = int
    SRV = str
    Date = str
    Value = str
    
    def __init__(self, ID, SRV, Date, Value):
        self.ID = ID
        self.SRV = SRV
        self.Date = Date
        self.Value = Value