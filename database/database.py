#Модуль взаимодействия с БД
from sqlalchemy.pool import QueuePool
from models.responce import Responce, Error
from models.logs import Logs
from models.statistics import StatisticNote
from hashlib import sha256
import time
import sqlalchemy

# def singleton(class_):
#     instances = {}
#     def getinstance(*args, **kwargs):
#         if class_ not in instances:
#             instances[class_] = class_(*args, **kwargs)
#         return instances[class_]
#     return getinstance

# @singleton
class Database:
    engine = sqlalchemy.engine
    Connect = sqlalchemy.engine.Connection
    Pool = QueuePool

    def __init__(self):
        try:
            self.engine = sqlalchemy.create_engine('sqlite:///notes.db', pool_size=5, max_overflow=1, poolclass=QueuePool)
            self.Pool = QueuePool(creator=self.engine.connect)
        except:
            print('[PANIC] init database')
            exit(1)
        print('[SUCCESS] init database')
    
    def TakeServer(self, srv) -> str:
        if srv == 1:
            return 'master'
        if srv == 2:
            return 'release'
        if srv == 3:
            return 'develop'
        if srv == 4:
            return 'database'

    def DatabaseINFO(self):
        print('[INFO] SQLAlchimy - ', sqlalchemy.__version__)
        print('[INFO] SQLite 3')
    
    # TakeConnect - function take a connection from sqlalchemy engine 
    def TakeConnect(self):
        return self.engine.connect

    # Check_database - fonction for check tables in database
    def Check_database(self):
        self.DatabaseINFO()
        # Check users table
        try:
            connect = self.TakeConnect()
            connect().execute('create table if not exists users (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email TEXT NOT NULL, pwd TEXT NOT NULL, firstname TEXT NOT NULL, lastname TEXT, middlename TEXT, UNIQUE(email));')
        except:
            print('[ERROR] check database. Check users table')
            exit(1)

        # Check logs table
        try:
            connect = self.TakeConnect()
            connect().execute('create table if not exists logs (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, srv INTEGER NOT NULL, date INTEGER NOT NULL, logs TEXT NOT NULL);')
        except:
            print('[ERROR] check database. Check status_note table')
            exit(1)

        # Check token_session
        try:
            connect = self.TakeConnect()
            connect().execute('create table if not exists token_session (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user INTEGER NOT NULL, token TEXT NOT NULL, date_update INTEGER NOT NULL, FOREIGN KEY (user) REFERENCES users (id));')
        except:
            print('[ERROR] check database. Check token_session table')
            exit(1)     
        
        print('[SUCCESS] check all tables database')

    # DefaultDataSet - function for set default data in satabase
    def DefaultDataSet(self):
        # Add default user
        try:
            connect = self.TakeConnect()
            res = connect().execute("select count(*) as 'all' from users;")
            res = res.fetchone()
            if res['all'] == 0:
                connect().execute("insert into users(email,pwd,firstname,lastname,middlename) values ('admin@mail.ru','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918','Иван','Иванов','Иванович');")
            else:
                print('[INFO] In your database exists a users.')
        except:
            print('[ERROR] add default. Check users table')
            exit(1)
        print('[SUCCESS] add defaults data in database')

    # SelectCheckLoginAndPassword - function for check email and password to a user
    def SelectCheckLoginAndPassword(self, email, pwd) -> Responce:
        try:
            connect = self.TakeConnect()
            password = sha256(pwd.encode('utf-8')).hexdigest()
            print(password)
            res = connect().execute("select id from users where email = '{email}' AND pwd = '{pwd}';".format(email=email,pwd=password))
            res = res.fetchone()
            if res['id'] is None:
                return Responce(Error('User does not exist'))
            else:
                token = email+str((time.time()))
                token = sha256(token.encode('utf-8')).hexdigest()
                try:
                    connect = self.TakeConnect()
                    connect().execute('delete from token_session WHERE id = {id}'.format(id=res['id']))
                    connect().execute("insert into token_session(user,token,date_update) values ('{id}','{token}',{date_update})".format(id=res['id'],token=token,date_update=int(time.time())))
                except:
                    return Responce(Error('Filed create token'))
                return Responce(token)
        except:
            return Responce(Error('Unknow database error'))

    # SelectDeleteToken - function for delete token of session
    def SelectDeleteToken(self, token) -> Responce:
        try:
            connect = self.TakeConnect()
            connect().execute("delete from token_session WHERE token = '{token}';".format(token=token))
        except:
            return Responce(Error('Unknow database error'))
        return Responce('Success')

    # SelectCheckToken - function for delete token of session
    def SelectCheckToken(self, token) -> Responce:
        try:
            connect = self.TakeConnect()
            res = connect().execute("select user as user from token_session WHERE token = '{token}';".format(token=token))
            res = res.fetchone()
            if res['user'] is None:
                return Responce(Error('Token doesn not exist'))
            return Responce(res['user'])
            # return Responce(1)
        except:
            return Responce(Error('Unknow database error'))

    # InsertLogs - function for create note in database
    def InsertLogs(self, srv, logs) -> Responce:
        try:
            connect = self.TakeConnect()
            time_create = int(time.time())
            connect().execute("insert into logs(srv,date,logs) values ({srv},{date},'{logs}');".format(
                srv=srv, date=time_create, logs=logs
            ))
            return Responce('Success!')
        except:
            return Responce(Error('Unknow database error'))



    # SelectNotes - function for request notes by filter
    def SelectLogs(self, filter) -> Responce:
        ListLogs = list()
        try:
            res = list()
            if filter == 'master':
                # SELECT 
                # logs.id AS id, 
                # CASE WHEN logs.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = logs.user) END AS user, 
                # strftime('%H:%M:%S',logs.date_create,'unixepoch','+3 hours') AS date_create, 
                # CASE WHEN logs.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',logs.date_start,'unixepoch','+3 hours') END  AS date_start, 
                # strftime('%H:%M:%S',logs.date_update,'unixepoch','+3 hours') AS date_update, 
                # logs.title AS title, logs.src AS note, status_note.name AS status 
                # FROM logs JOIN status_note ON status_note.id = logs.stat;
                connect = self.TakeConnect()
                res = connect().execute("SELECT logs.id, logs.srv, strftime('%H:%M:%S',logs.date,'unixepoch','+3 hours') AS date, logs.logs AS value FROM logs WHERE logs.srv = 1;")
            elif filter == 'release':
                # SELECT 
                # logs.id AS id, 
                # CASE WHEN logs.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = logs.user) END AS user, 
                # strftime('%H:%M:%S',logs.date_create,'unixepoch','+3 hours') AS date_create, 
                # CASE WHEN logs.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',logs.date_start,'unixepoch','+3 hours') END  AS date_start, 
                # strftime('%H:%M:%S',logs.date_update,'unixepoch','+3 hours') AS date_update, 
                # logs.title AS title, logs.src AS note, status_note.name AS status 
                # FROM logs JOIN status_note ON status_note.id = logs.stat WHERE logs.user = {id};
                connect = self.TakeConnect()
                res = connect().execute("SELECT logs.id, logs.srv, strftime('%H:%M:%S',logs.date,'unixepoch','+3 hours') AS date, logs.logs AS value FROM logs WHERE logs.srv = 2;")
            elif filter == 'develop':
                # SELECT 
                # logs.id AS id, 
                # CASE WHEN logs.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = logs.user) END AS user, 
                # strftime('%H:%M:%S',logs.date_create,'unixepoch','+3 hours') AS date_create, 
                # CASE WHEN logs.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',logs.date_start,'unixepoch','+3 hours') END  AS date_start, 
                # strftime('%H:%M:%S',logs.date_update,'unixepoch','+3 hours') AS date_update, 
                # logs.title AS title, logs.src AS note, status_note.name AS status 
                # FROM logs JOIN status_note ON status_note.id = logs.stat WHERE logs.stat = 2; 
                connect = self.TakeConnect()
                res = connect().execute("SELECT logs.id, logs.srv, strftime('%H:%M:%S',logs.date,'unixepoch','+3 hours') AS date, logs.logs AS value FROM logs WHERE logs.srv = 3;")
            elif filter == 'database':
                # SELECT 
                # logs.id AS id, 
                # CASE WHEN logs.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = logs.user) END AS user, 
                # strftime('%H:%M:%S',logs.date_create,'unixepoch','+3 hours') AS date_create, 
                # CASE WHEN logs.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',logs.date_start,'unixepoch','+3 hours') END  AS date_start, 
                # strftime('%H:%M:%S',logs.date_update,'unixepoch','+3 hours') AS date_update, 
                # logs.title AS title, logs.src AS note, status_note.name AS status 
                # FROM logs JOIN status_note ON status_note.id = logs.stat WHERE logs.stat = 4;
                connect = self.TakeConnect()
                res = connect().execute("SELECT logs.id, logs.srv, strftime('%H:%M:%S',logs.date,'unixepoch','+3 hours') AS date, logs.logs AS value FROM logs WHERE logs.srv = 4;")
            else:
                connect = self.TakeConnect()
                res = connect().execute("SELECT logs.id, logs.srv, strftime('%H:%M:%S',logs.date,'unixepoch','+3 hours') AS date, logs.logs AS value FROM logs")
            # if len(res) == 0:
            #     return Responce(Error('Empty list'))
            for row in res:
                print('load...',row.id,self.TakeServer(row.srv),row.date,row.value)
                ListLogs.append(Logs(
                    ID = row.id,
                    SRV = self.TakeServer(row.srv),
                    Date = row.date,
                    Value = row.value
                ))
                    
            return Responce(ListLogs)
        except:
            return Responce(Error('Unknow database error'))
